__author__ = 'root'

import random

##Clase Markov_Process -> Clase general con los 3 algoritmos , muestreo, forward y viterbi
## Constructor-> Vector inicial de probabilidad
##               Matriz de paso
##               Matriz de observaciones
##               Número de observaciones
class Markov_Process(object):

    def __init__(self, initialProb_vector, pass_matrix, observ_matrix, n):

        self.initialProb_vector = initialProb_vector
        self.pass_matrix = pass_matrix
        self.observ_matrix = observ_matrix
        self.state_number = n

    def muestreo(self):

        stateList = list()
        observ_list = list()
        previous_state=None
        #1st search in the initial probability vector
        sum = 0
        initProb = random.random()
        
        for i in range(0,len(self.initialProb_vector)):
            sum = sum + self.initialProb_vector[i]
            if sum >= initProb:
                stateList.append(i)
                previous_state=i
                obsProb= random.random()
                obs_vector= list(self.observ_matrix[i])
                sorted_obs_vector= list(self.observ_matrix[i])
                sorted_obs_vector.sort()
                
                sumProbObs= 0
                for j in sorted_obs_vector:
                    sumProbObs= sumProbObs+j
                    if sumProbObs >= obsProb:
                        observ_list.append(obs_vector.index(j))
                        break
                break
               

        #2nd search in pass matrix to find next state taking into a count the precicous State
        for i in range(1, self.state_number):

            passProb_vector = list(self.pass_matrix[previous_state])
            sorted_passProb_vector = list(self.pass_matrix[previous_state])
            sorted_passProb_vector.sort()
            
            passProb = random.random()
            sumProb = 0
       
            for j in sorted_passProb_vector:
                sumProb = sumProb + j
                if sumProb >= passProb:
                    stateList.append(passProb_vector.index(j))
                    previous_state=passProb_vector.index(j)
                    obs_vector= list(self.observ_matrix[passProb_vector.index(j)])
                    sorted_obs_vector= list(self.observ_matrix[passProb_vector.index(j)])
                    sorted_obs_vector.sort()
                    obsProb= random.random()                   
                                        
                    sumProbObs= 0
                    for k in sorted_obs_vector:
                        sumProbObs= sumProbObs+k
                        if sumProbObs >= obsProb:
                            observ_list.append(obs_vector.index(k))
                            break
                    break

        return list([stateList,observ_list])

    def filtering(self,obs_secuence):
        alfas_dic=dict()
        state_num= len(self.initialProb_vector)

        #Alpha0 all states
        for n in range(0,state_num): # alp0(s0),apl0(s1),alp0(s2)...
            first_observation=obs_secuence[0]
            alf= self.observ_matrix[n][first_observation] * self.initialProb_vector[n]
            alfas_dic[n]=alf

        #Alphas1..
        for i in obs_secuence[1:]: # Over observations for each alpha-> alp1,alp2,alp3..
            alp2_dic=dict()
            for k in range(0,state_num): # Over states for each aplha -> alp1(s0), alp1(s1)...
                prob= 0
                for j in range(0,state_num): # Sumatory about forward algorith.
                    prob= prob+self.pass_matrix[j][k]*alfas_dic[j]
                prob= prob*self.observ_matrix[k][i]
                alp2_dic[k]= prob
            alfas_dic=alp2_dic

        prob_list= max(alfas_dic.values())
        
        for x in alfas_dic.keys():
            if alfas_dic[x] == prob_list:
                best_state= list([x,prob_list])
                break;

        return best_state

    def decodification(self,obs_secuence):
        v_dict=dict()
        state_dict=dict()
        state_num= len(self.initialProb_vector)
        #v0 for all states
        for n in range(0,state_num): # v(s0),v(s1),v(s2)...
            first_observation=obs_secuence[0]
            v= self.observ_matrix[n][first_observation] * self.initialProb_vector[n]
            v_dict[n]=v
            state_dict[n]=list()

        #v1..
        for i in obs_secuence[1:]: # Over observation for each v-> v1,v2,v3..
            v2_dict=dict()

            for k in range(0,state_num): # Over state for each v -> v1(s0), v1(s1)...
                prob_list=list()

                for j in range(0,state_num):#Search the max  probability
                    prob_list.append(self.pass_matrix[j][k]*v_dict[j])
                max_prob=max(prob_list)
                v= max_prob*self.observ_matrix[k][i]

                v2_dict[k]= v
                state_dict[k].append(prob_list.index(max_prob))

            v_dict=v2_dict

        #Backward search best state way
        best_state_list=list()
        last_v_value= list(v_dict.values())
        best_state_list.append(last_v_value.index(max(last_v_value)))
        state_list=list(range(0,len(obs_secuence)-1))
        state_list.reverse()
        last_state=best_state_list[0]
        for i in state_list:
            sl=state_dict[last_state]
            state=sl[i]
            best_state_list.append(state)
            last_state=state
        best_state_list.reverse()
        return best_state_list

import random

##Clase Robot_Problem, que modela el problema
##Constructor-> longitud del mapa
##              anchura del mapa
##              lista de casillas bloqueadas
##              parámetro de error-> Epsilon
class Robot_Problem(object):

    def __init__(self, length, widht, bloqued_box, epsilon):

        self.length=length
        self.widht=widht
        self.bloqued_box=bloqued_box
        self.epsilon=epsilon
        self.box_num=length*widht
        self.all_movement='NSWE'
        self.all_observation=['','N','S','W','E','NS','NW','NE','SW','SE','WE','NSW','NSE','NWE','SWE','NSWE']
        #Map
        map_matrix=list()
        first=0
        last=length
        for x in range(0,widht):
            map_matrix.append(list(x for x in range(first,last)))
            first=last
            last=first+length

        self.map_matrix=map_matrix
        #Initial probability vector
        free_box_num= self.box_num - len(bloqued_box)

        self.initialProb_vector= list([1/self.box_num]*self.box_num)

        #Initial Box.
        self.free_box_list= list(x for x in range(0,self.box_num) if x not in bloqued_box)
        self.initial_Box=  random.choice(self.free_box_list)


    ##Function that return if a box is in the map 
    def in_of_range(self,s):
        return (s[0]>-1 and s[0]<self.widht) and (s[1]>-1 and s[1]<self.length)

    ##Function that return the movement that robot can apply
    def enable_movement(self,s):
        movement=''

        
        #North
        if self.in_of_range([s[0]-1,s[1]]):
            if (self.map_matrix[s[0]-1][s[1]] not in self.bloqued_box):
                movement+='N'

        #South
        if self.in_of_range([s[0]+1,s[1]]):
            if (self.map_matrix[s[0]+1][s[1]] not in self.bloqued_box):
                movement+='S'

        #West
        if self.in_of_range([s[0],s[1]-1]):
            if (self.map_matrix[s[0]][s[1]-1] not in self.bloqued_box):
                movement+='W'

        #East
        if self.in_of_range([s[0],s[1]+1]):             
            if (self.map_matrix[s[0]][s[1]+1] not in self.bloqued_box):
                movement+='E'

        return movement

    ##Function that return the movement that robot cannot apply
    def disable_movement(self,s):
        movement=''

        
        #North
        if self.in_of_range([s[0]-1,s[1]]):
            if (self.map_matrix[s[0]-1][s[1]] in self.bloqued_box):
                movement+='N'
        else: movement+='N'

        #South
        if self.in_of_range([s[0]+1,s[1]]):
            if (self.map_matrix[s[0]+1][s[1]] in self.bloqued_box):
                movement+='S'
        else: movement+='S'

        #West
        if self.in_of_range([s[0],s[1]-1]):
            if (self.map_matrix[s[0]][s[1]-1] in self.bloqued_box):
                movement+='W'
        else: movement+='W'
        #East
        if self.in_of_range([s[0],s[1]+1]):             
            if (self.map_matrix[s[0]][s[1]+1] in self.bloqued_box):
                movement+='E'
        else: movement+='E'

        return movement


    ##Function that return the neighbors of a box, box in coordenates
    def neighbors(self,s):
        box_index=[0,0] #[X,Y]
        neighbors_list=list()
        free_neighbors_list=list()
        #To pass the box with number (32), not coordenate [1,2]
        #for i in range(0,self.widht):

        #   if s in self.map_matrix[i]:
        #       box_index=[i,self.map_matrix[i].index(s)]
        #       break
        if self.in_of_range(s):
            #North
            N=list([s[0]-1,s[1]])
            #South
            S=list([s[0]+1,s[1]])
            #West
            W=list([s[0],s[1]-1])
            #East
            E=list([s[0],s[1]+1])
            actions=[N,S,W,E]

            for j in actions:
                if (self.in_of_range(j)):
                    if (self.map_matrix[j[0]][j[1]] not in self.bloqued_box):
                        neighbors_list.append(j)

        return neighbors_list

    ##Function that return the observation matrix
    def observation_Matrix(self):
        #Observation matrix:  /(1-Epsilon)^(good)* epsilon^(error)
        oM=list()

        for i in range(0,self.widht):
            
            for j in range(0,self.length):
                mov_list=self.enable_movement([i,j])
                box_obsv_list=list()
                for k in self.all_observation:
                    good=0
                    bad=0
                    for m in self.all_movement:

                        if m in mov_list and m in k:
                            good+=1
                        elif m in mov_list and m not in k:
                            bad+=1
                        elif m not in mov_list and m in k:
                            bad+=1
                        elif m not in mov_list and m not in k:
                            good+=1
                    prob= ((self.epsilon)**bad)* (1-self.epsilon)**good
                    box_obsv_list.append(prob)
                oM.append(box_obsv_list)

        return oM

    #Function that return the initial probability vector
    def initial_prob_vector(self):
        prob=1/len(self.free_box_list)
        initial_prob_vector=list()
        for i in range(0,self.box_num):
            if i not in self.bloqued_box:                
                initial_prob_vector.append(prob)
            else:
                initial_prob_vector.append(0)
        return initial_prob_vector
        

    ##Function that return the pass matrix
    def pass_Matrix(self):
    
        pM=list()
        prob=0
        for i in range(0,self.box_num):
            prob_list=list()
            neighbors_list=list([self.map_matrix[x[0]][x[1]] for x in self.neighbors(self.crd_of_box(i))])
            if len(neighbors_list)!=0:
                prob=1/len(neighbors_list)
            for j in range(0,self.box_num):
                if j in neighbors_list:
                    prob_list.append(prob)
                else:
                    prob_list.append(0)
            pM.append(prob_list)
        return pM

    ##Function that return the coordenates of a box, box in number(12)
    def crd_of_box(self,c):
        coord=list()
        for i in range(0,self.widht):
            if c in self.map_matrix[i]:
                coord=[i,self.map_matrix[i].index(c)]
                break
        return coord

    

rP= Robot_Problem(10,3,[5,11,15,20,25,29],0.2)
pM=rP.pass_Matrix()
oM=rP.observation_Matrix()
iV=rP.initial_prob_vector()

mP= Markov_Process(iV,pM,oM,5)
forward=mP.filtering([1,5,6,8,9])
decod=mP.decodification([1,5,6,8,9])
def manhattan_distance(X,Y):
    return abs(Y[0]-X[0]) + abs(Y[1]-X[1])

##Function to test the algorithtm with the robot localization problem
def test(number_of_test):
    manh_dist_list=list()
    coincidence_list=list()
    #Epsilon
    for e in list([0.0,0.02,0.05,0.1,0.2]):
        rP= Robot_Problem(16,4,[5,11,15,17,18,21,23,24,26,28,30,31,32,33,37,39,40,46,47,51,55,60],e)
        pM=rP.pass_Matrix()
        oM=rP.observation_Matrix()
        iV=rP.initial_prob_vector()        
        epsilon_list_manh=list()
        epsilon_list_coincidence=list()
        for o in range(1,41):
            forw_mean=0.0
            viter_mean=0.0
            forw_aux=list()
            viter_aux=list()
            for t in range(0,number_of_test):
                mP= Markov_Process(iV,pM,oM,o)
                ##Muestreo
                random_prob=mP.muestreo()
                oL=list(random_prob[1])
                #Mannhattan Distance                     
                forward=mP.filtering(oL)
                final_state_forward=forward[0]
                pto_X=rP.crd_of_box(oL[-1])
                pto_Y=rP.crd_of_box(final_state_forward)
                manhattan_dist= manhattan_distance(pto_X,pto_Y)
                forw_aux.append(manhattan_dist)
                #Viterbi
                decod=mP.decodification(oL)
                #Number of coincidence states
                good=0
                for k,m in zip(random_prob[0],decod):
                    if k==m:
                        good+=1
                viter_aux.append(good/o)

            forw_mean= sum(forw_aux)/len(forw_aux)
            viter_mean= sum(viter_aux)/len(viter_aux)
            epsilon_list_manh.append(forw_mean)
            epsilon_list_coincidence.append(viter_mean)    
        
        manh_dist_list.append(epsilon_list_manh)
        coincidence_list.append(epsilon_list_coincidence)
    
    print('Forward->',manh_dist_list)
    print('Viterbi->',coincidence_list)
    

    

    
